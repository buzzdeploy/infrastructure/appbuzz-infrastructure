#!/bin/bash
# Used for deleting artifacts stored in the Linode Bucket: Needs python container

# Set variables from terraform state
uniqueid="s"$(grep "unique_number" main.tf |awk -F'\"' '{print $2}')
configname="${uniqueid}config"

# Push to config to an S3 bucket
linode-cli obj rm k3sfiles ${configname} --cluster us-east-1
linode-cli obj rm k3sfiles ${uniqueid}linode.hosts --cluster us-east-1
