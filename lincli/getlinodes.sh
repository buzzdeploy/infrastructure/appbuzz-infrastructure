#!/usr/bin/bash

ANSIBLE_HOME=../ansible
# Get machines from Linode
linode-cli linodes list --json > listjson.json
# Create host file from inventory hosts
python3.9 invcreate.py listjson.json \
 $ANSIBLE_HOME/linodeinv.hosts \
 98 \
 $ANSIBLE_HOME/linodelogin.hosts

ls -lsa $ANSIBLE_HOME

cat $ANSIBLE_HOME/linodeinv.hosts $ANSIBLE_HOME/base.hosts \
$ANSIBLE_HOME/linodelogin.hosts \
> $ANSIBLE_HOME/linode.hosts

# Required to use the local ansible cfg in the folder
chmod 700 $ANSIBLE_HOME
