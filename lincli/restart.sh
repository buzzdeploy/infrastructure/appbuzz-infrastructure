#!/bin/bash
# Restart linodes based on their unique number for deployment found in ../main.tf

# uniqueid=$1
uniqueid="s"$(grep "unique_number" ../main.tf |awk -F'\"' '{print $2}')
systems=$(linode-cli linodes list|grep $uniqueid| awk '{print $2}')

echo $systems

# Perform reboots
for i in ${systems[@]}; do
    linode-cli linodes reboot $i
done;

# Reboot command takes up to 15 seconds in testiong to start rebooting
sleep 15s

while :
do
    running=true

    linode-cli linodes list|grep $uniqueid|awk '{print $12}'
    runcheck=$(linode-cli linodes list|grep $uniqueid|awk '{print $12}')
    for i in ${runcheck[@]}; do
        # if [ $i != "running" ]; then
        if [ $i != [32mrunning[0m ]; then
            echo "Systems are not running yet"
            running=false
        fi
    done

    echo "Systems running: $running"

    if [ $running == "true" ]; then
        echo "Systems are online"
        break
    fi

    # Wait to not flood the api with polling
    sleep 5s
done

# Wait for OS to accept ssh connections after reboot
echo "Waiting 20 seconds for the systems to accept an ssh connection"
sleep 20s

echo "Systems online and booted"

